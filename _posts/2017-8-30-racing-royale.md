---
layout: post
title: Racing Royale 2P
description: Challenge your friend in a 2P racing
image: assets/images/project_racing_royale.png
link: http://bit.ly/racingroyale
type: portfolio
---

An exciting 2P split viewport based game that contains almost all necessary vehicular functional controller with NOS system. Surpass all laps the right way and win the game.
Enjoy playing it more with your friends or rival.

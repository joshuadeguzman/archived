---
layout: post
title: Electionify
description: A dynamic and responsive web based election management tool
image: assets/images/project_electionify.png
link: http://bit.ly/electionify
featured: true
type: portfolio
---

Electionify is a robust election management system that lets clients easily handle the events by just managing the users and parties, and let us do the rest.

Features include:
* Event management
* User management
* Organisation Management
* Party management
* Departmental/Grouping management
* Analytics
* Results management

Do you need some help, <a href="http://jodeio.github.io#contact">shoot me a message</a>

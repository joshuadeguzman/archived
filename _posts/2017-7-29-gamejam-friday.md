---
layout: blog
title: 'GameJam Friday: Arithmetify'
description: '
It has been a horrific week for me continuing my projects after becoming part of a gullible public believing that our internet service provider will fix our internet connection within 24-48 hrs.
 
So I decided not to waste the Friday doing nothing again, instead I tried creating a simple game using Android Studio created with Java programming language for both mobile and wear. Link to the Game: Arithmetify
'
image: assets/images/blog_gamejam_cover.png
link: http://bit.ly/arithmetify
git: null
type: blog

# images collection
images:
  - image:
        title: 'Mobile: Game Screen'
        path: assets/images/blog_gamejam_ss_1.png
  - image:
        title: 'Wear: Gameover Indicator'
        path: assets/images/blog_gamejam_ss_2.png
  - image:
        title: 'Wear: Game Screen'
        path: assets/images/blog_gamejam_ss_3.png
---

It has been a horrific week for me continuing my projects after becoming part of a gullible public believing that our internet service provider will fix our internet connection within 24-48 hrs.
 
So I decided not to waste the Friday doing nothing again, instead I tried creating a simple game using Android Studio created with Java programming language for both mobile and wear.
 
At first I thought it would be a task that can be done within 3-5 hours, but as I went through developing the game it took me almost 9 hours. Including the production of assets, so far I haven’t added background music yet.
 
This app has a simple gameplay, you just have to check within a given time whether the given equation is valid or not. This aims to develop your fast and accurate arithmetic solving skills.
 
I am not claiming this game is original, but rest assured all source codes and assets are done solely by me.
 
Do not let an interrupted internet connection cripple to lower your productivity; but also don’t forget to report to your respective ISP when you're already bothered.

Best,

Joshua

---
layout: post
title: Arithmetify
description: A fast paced arithmetic challenge game
image: assets/images/project_arithmetify.png
link: http://bit.ly/arithmetify
featured: true
type: portfolio
---

A fun and exciting educational game created to enhance your fast and accurate solving skills.

Are you ready to take the challenge?

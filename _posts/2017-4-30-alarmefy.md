---
layout: post
title: Alarmefy
description: Provides you a productive way of waking up
image: assets/images/project_alarmefy.png
link: http://bit.ly/alarmefy
featured: true
type: portfolio
---

Are you having an unproductive way of starting your day? Then this is the app for you!
Alarmefy aims to wake you up with annoying challenges before you can turn off your alarm. Simply add new alarm with a default challenge, save it and good to go. It will remind you of something you don't want to miss out in your life, especially enjoying the early deeds in the morning.

Enjoy the latest updates from well known news and blog sources all across the globe as they bring you their latest stories.

Be productive everyday, read news, know the day's weather of most cities and use Alarmefy everyday!

Features:
* Solve Math: Solve arithmetic equations to dismiss alarm
* Shake Phone: Shake your phone to turn off the annoying alarm
* NewsOnTheGo: Get the latest news updates from more than 30+ respected and well known range of news sources and blogs.
* WeatherSearch: Get the weather information for almost all the cities across the globe.
* Customize your own alarm preferences
* Minimalist User Interface Design
* Fast and easy to use

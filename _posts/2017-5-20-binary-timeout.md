---
layout: post
title: Binary Timeout
description: A nerve wrecking binary puzzle game
image: assets/images/project_binary_timeout.png
link: http://bit.ly/binarytimeout
featured: true
type: portfolio
---

[Beta Release]

Binary Timeout offers you unique way of enjoying and playing computer binary with puzzles in your mobile phone. Enjoy playing the game while practicing and improving your mathematical, logical solving capabilities through the gameplay offered.

Special Feature:
* BinaDoku - Solve binary like solving sudoku! Solve with the fastest times among your friends
- Working Modes (3x3, 4x4, 5x5)
- Upcoming Modes (6x6-8x8!!!)

Enjoy the following game modes:
* Timed - Endless solving of time thrilling solving for binary computations
- Modes (30s, 60s, 120s)

* Survival - Survive and beat the high scores until the timer (background) lasts
- Modes (Easy, Moderate, Difficult)

Upcoming Features:
* Facebook/Google Play Games Integration for Leaderboards
* Interactive Help Tutorials
* More features and modes to be unlocked!

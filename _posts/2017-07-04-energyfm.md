---
layout: post
title: EnergyFM Mobile
description: A real time mobile streaming application for the infamous EnergyFM radio station
image: assets/images/project_energyfm.png
link: http://bit.ly/energyfm-mobile
featured: true
type: portfolio
---

EnergyFM Manila 106.7 is a mobile online streaming application of EnergyFM Manila. Listen to your favorite Pangga DJs as the app brings you closer to them. Get the latest radio station news and events, social media updates, watch previous video playlists and more!

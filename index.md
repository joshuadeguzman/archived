---
layout: home
title: Home
landing-title: 'Howdy, Welcome!'
description: null
image: null
author: null
---

<b>Joshua is an enthusiastic software engineer.
<br>Experienced with industry based toolsets and practices, project management, test automation and test driven development.</b>
<br>Have strong work ethic, team oriented, flexible and dependable.

<b>Worked with contract based projects for almost 3 years now both for local and abroad based clients alongside taking up courses as a student.</b>
<br>Already worked for some contract based projects alone, but as always, works best with a team.

<b>Engaged in Pro-Bono/Volunteering works to communities promoting education, software and technology advancement. </b>
<br>@GithubDevelopersProgram @CODERSGuild @VRPhilippines @TechCaravan @CodingGirls
